Vagrant.configure("2") do |config|
  config.ssh.username = "rancher"
  config.vm.guest = :linux
  config.vm.synced_folder ".", "/vagrant", disabled: true

  config.vm.provider :virtualbox do |v, override|
    v.check_guest_additions = false
  end
end
